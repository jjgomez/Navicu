<?
include ("../conexion/conexion.php");
error_reporting(0);
session_start();
if($_SESSION["ok"]!=true){
    
    ?>
      <script>
        alert('Debes Iniciar la Sesion!');
        window.location="index.html";
      </script>
    <?
    return;
}


/*Busqueda*/
$habitaciones="SELECT h.id_habitacion,h.nombre,h.tarifa,h.maximo_personas,h.created,e.nombre as establecimiento,u.usuario FROM habitaciones h 
INNER JOIN establecimientos e ON h.id_establecimiento=e.id_establecimiento   
INNER JOIN usuarios u ON u.id_usuario=e.id_usuario WHERE h.is_active='1' and u.id_usuario='".$_SESSION['usuario']."' ORDER BY h.nombre asc";
$result=$mysqli->query($habitaciones);

$establecimiento="SELECT id_establecimiento,nombre FROM establecimientos WHERE is_active='1' and id_usuario='".$_SESSION['usuario']."' ORDER BY nombre asc";
$result2=$mysqli->query($establecimiento);

?>
<!DOCTYPE html>
<html>
    <head>
          <? include '../includes/title.php';?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme-change-size.css">

        <!-- Datatables -->
        <link rel="stylesheet" media="screen" href="../assets/css/DT_bootstrap.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script type="text/javascript" src="js/html5shiv.js"></script>
           <script type="text/javascript" src="js/respond.min.js"></script>
        <![endif]-->

<script>
 /*Funcion numeros decimales*/
function format(input)
{
var num = input.value.replace(/\./g,'');
if(!isNaN(num)){
if(num.length>2)
{
num=num.substring(0,num.length-2)+'.'+num.substring(num.length-2);
}
input.value = num;
}

else{ alert('Solo se permiten numeros');
input.value = input.value.replace(/[^\d\.]*/g,'');
}
}
</script>


<script language=""="JavaScript">
         function conMayusculas(field) {
            field.value = field.value.toUpperCase()
        }
</script>

<script type="text/javascript">
        // Solo permite ingresar numeros.
       function soloNumeros(e){
       var key = window.Event ? e.which : e.keyCode
       return (key >= 48 && key <= 57)
        }
</script>

    </head>
     <? include '../includes/header.php';?>

        <? include '../includes/menu.php';?>

         <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Habitaciones</h1>
                            </div>
                        </div>
                    </div>
                <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title"><a href="#"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#newReg1">
                    <i class="glyphicon glyphicon-ok-sign"></i> CRUD</button></a></div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                         <form class="form-horizontal" action="../controlador/controlador.php" method="post">
                                           <input type="text" name="habitaciones"   value="01" hidden="hidden" readonly/>
                                        <fieldset>
                                            <legend>Registro de Habitaciones</legend>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="establecimiento">Establecimiento</label>
                                                <div class="col-lg-10">
                                                     <select  class="form-control chzn-select" tabindex="2"  name="id_establecimiento">
                                             <? while ($resultado=$result2->fetch_array(MYSQLI_ASSOC)){
                                            echo "<option value='".$resultado['id_establecimiento']."'> ".$resultado['nombre']."</option>";
                                              }?>
                                               </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="nombre">Nombre</label>
                                                <div class="col-lg-10">
                                                <input class="form-control" id="nombre" name="nombre" type="text" onChange="conMayusculas(this)" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="tarifa">Tarifa</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="tarifa" name="tarifa" type="text" pattern="[0-9]+(\.[0-9][0-9]?)" title="Este Campo Requiere Digitos"  onkeyup="format(this)" onchange="format(input)" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="maximo_personas">Maximo Personas</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="maximo_personas" name="maximo_personas" type="text" onKeyPress="return soloNumeros(event)" required>
                                                </div>
                                            </div> 
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            <button type="reset" class="btn btn-default">Cancelar</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!--Inicio de modal-->
        <div class="modal fade" id="newReg1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                   <div class="modal-body">
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px;">
                                    <thead>
                                            <tr>
                                                 <th>Establecimiento</th>
                                                 <th>Usuario</th>
                                                 <th>Nombre Habitaciones</th>
                                                 <th>Tarifa</th>
                                                 <th>Maximo Personas</th>
                                                 <th>Creado</th>
                                                 <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <?$i=1;
                           while ($row=$result->fetch_array(MYSQLI_ASSOC)){
    ?>                   
                                            <tr class="odd gradeX">
                                                <td><?php echo $row['establecimiento'];?></td>
                                                <td><?php echo $row['usuario'];?></td>
                                                <td><?php echo $row['nombre'];?></td>
                                                <td><?php echo $row['tarifa'];?></td>
                                                <td class="center"><?php echo $row['maximo_personas'];?></td>
                                                <td class="center"><?php echo $row['created'];?></td>
                                                <td class="center"><a href="../controlador/controlador.php?hid=<?php echo $row['id_habitacion'];?>">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Eliminar
                                                        </button>
                                                    </a></td>
                                                </tr>
                                                 <? $i ++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    
                  </div>
                                    </div>
                                </div>
                            </div>
                       </div>
            <!---FIN-->
                   </div>
            </div>
       </div>
</div>

</div>
                </div>

              
        <!-- footer -->
       <? include '../includes/footer.php';?>

        <script type="text/javascript" src="../assets/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="../assets/js/bootstrap-admin-theme-change-size.js"></script>
        <script type="text/javascript" src="../assets/vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/js/DT_bootstrap.js"></script>
    </body>
</html>
