<?
error_reporting(0);
session_start();
include ("../conexion/conexion.php");
if($_SESSION["ok"]!=true){
    
    ?>
      <script>
        alert('Debes Iniciar la Sesion!');
        window.location="index.html";
      </script>
    <?
    return;
}

/*Busqueda*/
$habitaciones="SELECT h.id_habitacion,h.nombre,h.tarifa,h.maximo_personas,h.created,e.nombre as establecimiento,u.usuario FROM habitaciones h 
INNER JOIN establecimientos e ON h.id_establecimiento=e.id_establecimiento   
INNER JOIN usuarios u ON u.id_usuario=e.id_usuario WHERE h.is_active='1' and u.id_usuario='".$_SESSION['usuario']."' ORDER BY h.nombre asc";
$result=$mysqli->query($habitaciones);
?>
<!DOCTYPE html>
<html>
    <head>
        <? include '../includes/title.php';?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="../assets/vendors/easypiechart/jquery.easy-pie-chart.css">
        <link rel="stylesheet" media="screen" href="../assets/vendors/easypiechart/jquery.easy-pie-chart_custom.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script type="text/javascript" src="js/html5shiv.js"></script>
           <script type="text/javascript" src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    
       <? include '../includes/header.php';?>

        <? include '../includes/menu.php';?>
                

                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1></h1>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-success bootstrap-admin-alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <h4>Navicu</h4>
                                Reservas de Alojamientos Online
                            </div>
                        </div>
                    </div>

                       <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px;">
                                    <thead>
                                            <tr>
                                                 <th>#</th>
                                                 <th>Establecimiento</th>
                                                 <th>Usuario</th>
                                                 <th>Nombre Habitacion</th>
                                                 <th>Tarifa</th>
                                                 <th>Maximo Personas</th>
                                                 <th>Creado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <?$i=1;
                           while ($row=$result->fetch_array(MYSQLI_ASSOC)){
    ?>                   
                                            <tr class="odd gradeX">
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $row['establecimiento'];?></td>
                                                <td><?php echo $row['usuario'];?></td>
                                                <td><?php echo $row['nombre'];?></td>
                                                <td><?php echo $row['tarifa'];?></td>
                                                <td class="center"><?php echo $row['maximo_personas'];?></td>
                                                <td class="center"><?php echo $row['created'];?></td>
                                                </tr>
                                                 <? $i ++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
       <script type="text/javascript" src="../assets/js/jquery-2.0.3.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="../assets/js/bootstrap-admin-theme-change-size.js"></script>
        <script type="text/javascript" src="../assets/vendors/easypiechart/jquery.easy-pie-chart.js"></script>

        <script type="text/javascript">
            $(function() {
                // Easy pie charts
                $('.easyPieChart').easyPieChart({animate: 1000});
            });
        </script>
    </body>
</html>