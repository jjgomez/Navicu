<?
include ("../conexion/conexion.php");
error_reporting(0);
session_start();
if($_SESSION["ok"]!=true){
    
    ?>
      <script>
        alert('Debes Iniciar la Sesion!');
        window.location="index.html";
      </script>
    <?
    return;
}


/*Busqueda*/
$establecimiento="SELECT e.id_establecimiento,e.nombre,e.ubicacion,e.tipo_establecimiento,e.fecha_inauguracion,e.direccion,e.created,u.usuario FROM establecimientos e 
INNER JOIN usuarios u ON u.id_usuario=e.id_usuario WHERE e.is_active='1' and u.id_usuario='".$_SESSION['usuario']."' ORDER BY e.nombre asc";
$result=$mysqli->query($establecimiento);

$usuarios="SELECT id_usuario,usuario FROM usuarios WHERE is_active='1' and id_usuario='".$_SESSION['usuario']."' ORDER BY nombre asc";
$result2=$mysqli->query($usuarios);

?>
<!DOCTYPE html>
<html>
    <head>
          <? include '../includes/title.php';?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

       <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="../assets/css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="../assets/vendors/bootstrap-datepicker/css/datepicker.css">
        <link rel="stylesheet" media="screen" href="../assets/css/datepicker.fixes.css">
        <link rel="stylesheet" media="screen" href="../assets/vendors/uniform/themes/default/css/uniform.default.min.css">
        <link rel="stylesheet" media="screen" href="../assets/css/uniform.default.fixes.css">
        <link rel="stylesheet" href="../assets/plugins/datepicker/css/datepicker.css" />


        <script language=""="JavaScript">
         function conMayusculas(field) {
            field.value = field.value.toUpperCase()
        }
        </script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script type="text/javascript" src="js/html5shiv.js"></script>
           <script type="text/javascript" src="js/respond.min.js"></script>
        <![endif]-->
    </head>
        <? include '../includes/header.php';?>
        
        <? include '../includes/menu.php';?>

            <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Establecimientos</h1>
                            </div>
                        </div>
                    </div>
                <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title"><a href="#"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#newReg1">
                    <i class="glyphicon glyphicon-ok-sign"></i> CRUD</button></a></div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                         <form class="form-horizontal" action="../controlador/controlador.php" method="post">
                                           <input type="text" name="establecimieto"   value="01" hidden="hidden" readonly/>
                                        <fieldset>
                                            <legend>Registro de Establecimientos</legend>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="usuario">Usuario</label>
                                                <div class="col-lg-10">
                                                    <select  class="form-control chzn-select" tabindex="2"  name="id_usuario">
                                                         <? while ($resultado=$result2->fetch_array(MYSQLI_ASSOC)){
                                               echo "<option value='".$resultado['id_usuario']."'> ".$resultado['usuario']."</option>";
                                                     }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="nombre">Nombre</label>
                                                <div class="col-lg-10">
                                                <input class="form-control" id="nombre" name="nombre" type="text" onChange="conMayusculas(this)" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="ubicacion">Ubicacion</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="ubicacion" name="ubicacion" type="text" onChange="conMayusculas(this)" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="tipo_establecimiento">Tipo establecimiento</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="tipo_establecimiento" name="tipo_establecimiento" type="text" onChange="conMayusculas(this)" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label" for="fecha_inauguracion">Fecha inauguracion</label>
                                                <div class="col-lg-10">
                                                <input class="form-control datepicker" id="date01" id="fecha_inauguracion" name="fecha_inauguracion" type="text" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-lg-2 control-label" for="direccion">Direccion</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="direccion" name="direccion" type="text" onChange="conMayusculas(this)" required>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            <button type="reset" class="btn btn-default">Cancelar</button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!--Inicio de modal-->
        <div class="modal fade" id="newReg1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                   <div class="modal-body">
                        <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px;">
                                    <thead>
                                            <tr>
                                                 <th>Nombre</th>
                                                 <th>Ubicacion</th>
                                                 <th>Tipo Establecimiento</th>
                                                 <th>Fecha Inauguracion</th>
                                                 <th>Direccion</th>
                                                 <th>Usuario</th>
                                                 <th>Creado</th>
                                                 <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <?$i=1;
                           while ($row=$result->fetch_array(MYSQLI_ASSOC)){
    ?>                   
                                            <tr class="odd gradeX">
                                                <td><?php echo $row['nombre'];?></td>
                                                <td><?php echo $row['ubicacion'];?></td>
                                                <td><?php echo $row['tipo_establecimiento'];?></td>
                                                <td class="center"><?php echo $row['fecha_inauguracion'];?></td>
                                                <td class="center"><?php echo $row['direccion'];?></td>
                                                <td><?php echo $row['usuario'];?></td>
                                                <td class="center"><?php echo $row['created'];?></td>
                                                <td class="center"><a href="../controlador/controlador.php?eid=<?php echo $row['id_establecimiento'];?>">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Eliminar
                                                        </button>
                                                    </a></td>
                                                    </tr>
                                                 <? $i ++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    
                  </div>
                                    </div>
                                </div>
                            </div>
                       </div>
            <!---FIN-->
                   </div>
            </div>
       </div>
</div>

</div>
                </div>
        <!-- footer -->
       <? include '../includes/footer.php';?>

       <script type="text/javascript" src="../assets/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../assets/vendors/boostrap3-typeahead/bootstrap3-typeahead.min.js"></script>
        <script src="../assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.datepicker').datepicker();          
            });
        </script>
    </body>
</html>
