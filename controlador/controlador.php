<?
include ("../conexion/conexion.php");

//fecha y Hora//
date_default_timezone_set("America/Caracas" ) ;
$hora =(time('H') - 3600*date('I'));
$hora2 = date('H:i:s',$hora);
$fecha = date('Y-m-d');

?>

<html>
    <head>
        <script type="text/javascript" src="../assets/lib/alertify.js"></script>
        <link rel="stylesheet" href="../assets/themes/alertify.core.css" />
        <link rel="stylesheet" href="../assets/themes/alertify.default.css" />
        <script>
            function ok(){
                alertify.success("Registro Exitoso"); 
                return false;
            }
            function error1(){
                alertify.error("Datos Incorrectos"); 
                return false; 
            }
            function eliminar(){
                alertify.error("Registro Eliminado Con Exitos.!"); 
                return false; 
            }
             function error2(){
                alertify.error("Ya existe un Registro con esos Datos"); 
                return false; 
            }
             function error3(){
                alertify.error("Contraseñas No Coinciden"); 
                return false; 
            }
            function error4(){
                alertify.error("Usuario ya Existe"); 
                return false; 
            }
             function error5(){
                alertify.error("Cedula ya Existe"); 
                return false; 
            }
            function error6(){
                alertify.error("Establecimiento ya Existe"); 
                return false; 
            }
             function error7(){
                alertify.error("Habitacion ya Existe"); 
                return false; 
            }
        </script>
    </head>
</html>
<?
///////////Registrar Usuario Hotelero/////////////
if (isset($_POST['usuario']))
{



$validacion1="SELECT usuario  FROM usuarios  WHERE usuario='".$_POST['usuario']."' AND is_active='1'";
$result1=$mysqli->query($validacion1);
$row1=$result1->fetch_array(MYSQLI_ASSOC);
$row_cnt1 = $result1->num_rows;

$validacion2="SELECT cedula  FROM usuarios  WHERE cedula='".$_POST['cedula']."' AND is_active='1'";
$result2=$mysqli->query($validacion2);
$row2=$result2->fetch_array(MYSQLI_ASSOC);
$row_cnt2 = $result2->num_rows;


if ($_POST['clave']!=$_POST['clave1']) { ?>

 <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); error3();           
</script>
<?
}  elseif ($row_cnt1!=0) { ?>

 <script languaje="javascript">
//un alert
alertify.alert("<b>Usuario <? echo $row1['usuario'];  ?> ya Existe</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); error4();           
</script>

<? } elseif($row_cnt2!=0){ ?>

 <script languaje="javascript">
//un alert
alertify.alert("<b>Cedula <? echo $row2['cedula'];  ?> ya Existe</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); error5();           
</script>
<? } else{


///realizamos la conversion del formato de fecha///
$fecha_nacimiento=date("Y-m-d",strtotime($_POST['fecha_nacimiento']));

////Realizamos el INSERT
$x="INSERT INTO usuarios VALUES ('','".$_POST['usuario']."','".$_POST['nombre']."','".$_POST['apellido']."','".$_POST['cedula']."','".$fecha_nacimiento."','".$_POST['correo']."',sha1('".$_POST['clave']."'),'".$fecha."','1')";
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); error1();           
</script>
<?
		
$mysqli->close();

     }
  }

}

///////////Eliminar Usuario/////////////
if (isset($_GET['uid']))
{

////Realizamos el INSERT
$x=("UPDATE usuarios SET is_active='0' WHERE id_usuario='".$_GET['uid']."'");
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/usuarios.php';
}); error1();           
</script>
<?
    
$mysqli->close();

     }
  }

///////////Registrar Establecimientos/////////////
if (isset($_POST['establecimieto']))
{

$validacion1="SELECT nombre  FROM establecimientos  WHERE nombre='".$_POST['nombre']."' AND is_active='1'";
$result=$mysqli->query($validacion1);
$row=$result->fetch_array(MYSQLI_ASSOC);
$row_cnt = $result->num_rows;

if ($row_cnt!=0) { ?>

 <script languaje="javascript">
//un alert
alertify.alert("<b>Establecimiento <? echo $row['nombre'];  ?> ya Existe</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/establecimientos.php';
}); error6();           
</script>
<?
}

else{

///realizamos la conversion del formato de fecha///
$fecha_inauguracion=date("Y-m-d",strtotime($_POST['fecha_inauguracion']));
////Realizamos el INSERT
$x="INSERT INTO establecimientos VALUES ('','".$_POST['id_usuario']."','".$_POST['nombre']."','".$_POST['ubicacion']."','".$_POST['tipo_establecimiento']."','".$fecha_inauguracion."','".$_POST['direccion']."','$fecha','1')";
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/establecimientos.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/establecimientos.php';
}); error1();           
</script>
<?
    
$mysqli->close();

     }

  }

}

///////////Eliminar Establecimientos/////////////
if (isset($_GET['eid']))
{

////Realizamos el INSERT
$x=("UPDATE establecimientos SET is_active='0' WHERE id_establecimiento='".$_GET['eid']."'");
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/establecimientos.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/establecimientos.php';
}); error1();           
</script>
<?
    
$mysqli->close();

     }
  }

///////////Registrar Habitaciones/////////////
if (isset($_POST['habitaciones']))
{


$validacion1="SELECT nombre  FROM habitaciones  WHERE nombre='".$_POST['nombre']."' AND is_active='1'";
$result=$mysqli->query($validacion1);
$row=$result->fetch_array(MYSQLI_ASSOC);
$row_cnt = $result->num_rows;

if ($row_cnt!=0) { ?>

 <script languaje="javascript">
//un alert
alertify.alert("<b>Habitacion <? echo $row['nombre'];  ?> ya Existe</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/habitaciones.php';
}); error7();           
</script>
<?
}
 else {

////Realizamos el INSERT
$x="INSERT INTO habitaciones VALUES ('','".$_POST['id_establecimiento']."','".$_POST['nombre']."','".$_POST['tarifa']."','".$_POST['maximo_personas']."','$fecha','1')";
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/habitaciones.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/habitaciones.php';
}); error1();           
</script>
<?
    
$mysqli->close();

     }

   }

}

///////////Eliminar Habiticiones/////////////
if (isset($_GET['hid']))
{

////Realizamos el INSERT
$x=("UPDATE habitaciones SET is_active='0' WHERE id_habitacion='".$_GET['hid']."'");
$mysqli->query($x);

 if($x){
          ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Solicitud Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/habitaciones.php';
}); ok();           
</script>
<?
}else{
  ?>
   <script languaje="javascript">
//un alert
alertify.alert("<b>Su Solicitud No Fue Generada</b>", function () {
//aqui introducimos lo que haremos tras cerrar la alerta.
location.href = '../source/habitaciones.php';
}); error1();           
</script>
<?
    
$mysqli->close();

     }
  }